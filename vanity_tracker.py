#!/usr/bin/python
# -*- coding: utf-8 -*-

import sqlite3
import collections
import logging
import datetime
import sys
import re
import os
import pprint
import matplotlib.pyplot as plt

con = None

stream = logging.StreamHandler()
stream.setLevel(logging.DEBUG)
formatter_simple = logging.Formatter(
    '%(asctime)s - %(levelname)s - %(message)s')
stream.setFormatter(formatter_simple)
logger = logging.getLogger(__name__)
logger.addHandler(stream)
logger.setLevel(logging.DEBUG)
logger.info('test')

#PATH = '/run/shm/twintrim-vanity/'
PATH = '/run/shm/files/'

with sqlite3.connect('vanity.db') as con:

    cur = con.cursor()
    cur.execute('SELECT SQLITE_VERSION()')

    data = cur.fetchone()

    logger.info("SQLite version: %s", data)

    cur.execute("CREATE TABLE IF NOT EXISTS Downloads(Id INTEGER primary key autoincrement, datetime INT UNIQUE, date_string TEXT, v0dot2 INT, v0dot5 INT, v0dot6 INT, v0dot9 INT, v0dot9dot1 INT, v0dot10 INT, v0dot11 INT, v0dot12 INT, v0dot12dot1 INT)")

    regex = re.compile(r'^ *twintrimmer-(.*?)\.tar\.gz +([-0-9]{10}) +([0-9]+)')
    for name in os.listdir(PATH):
        logger.debug('Current file: %s', name)
        try:
            dt = datetime.datetime.strptime(name, "twintrimmer-stats-%Y-%m-%d-%H:%M:%S.txt")
        except ValueError:
            continue
        timestamp = dt.strftime('%Y-%m-%d %H:%M:%S')
        logger.debug('timestamp: %s', timestamp)
        unix_timestamp = int((dt - datetime.datetime(1970,1,1)).total_seconds())
        logger.debug('unix timestamp: %s', unix_timestamp)

        with open(os.path.join(PATH, name)) as file:
            download_counts = collections.defaultdict(int)
            for line in file.readlines()[:-2]:
                logger.debug('line: %s', line)
                match = regex.match(line)
                if match:
                    version, _, download_count = match.groups()
                    logger.debug('groups: %s', repr(match.groups()))
                    download_counts['v' + 'dot'.join(version.split('.'))] = download_count
                    logger.debug('key: %s \t value: %s', 'v' + 'dot'.join(version.split('.')), download_count)
                else:
                    logger.warning('Line doesn\'t match: %s', line)

            columns = ['v0dot2', 'v0dot5', 'v0dot6', 'v0dot9', 'v0dot9dot1', 'v0dot10', 'v0dot11', 'v0dot12', 'v0dot12dot1']
            assert not set(download_counts.keys()) - set(columns)
            download_counts = ', '.join([str(download_counts[item]) for item in columns])
            logger.debug('Download counts: %s', download_counts)
            try:
                cur.execute("INSERT INTO Downloads VALUES(null, {unix_timestamp}, '{timestamp}', {download_counts})".format(**locals()))
            except sqlite3.IntegrityError:
                logger.info('Timestamp already exists: %s', unix_timestamp)

    #cur.execute("INSERT INTO Downloads VALUES(null, 1444526221, '2015-10-11-01:17:01', 1190, 821, 316, 194, 137, 141, 128, 75)")
    cur.execute('SELECT * FROM Downloads ORDER BY datetime')

    data = cur.fetchall()
    #pprint.pprint(data)
    columns = ['id', 'unix_timestamp', 'Timestamp', 'v0dot2', 'v0dot5', 'v0dot6', 'v0dot9', 'v0dot9dot1', 'v0dot10', 'v0dot11', 'v0dot12', 'v0dot12dot1']
    table = collections.defaultdict(list)
    for line in data:
        for key, value in zip(columns, line):
            table[key].append(value)
    pprint.pprint(table)
    x = [item - min(table['unix_timestamp']) for item in table['unix_timestamp']]
    plt.plot(x, table['v0dot2'], '-', x, table['v0dot5'], '--',
             x, table['v0dot6'], '--', x, table['v0dot9'], '--',
             x, table['v0dot9dot1'], '--', x, table['v0dot10'], '--',
             x, table['v0dot11'], '--', x, table['v0dot12'], '--',
             x, table['v0dot12dot1'], '--')
    plt.show()
